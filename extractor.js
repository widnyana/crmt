"use strict";

const urllib = require("url");
const request = require("request");
const cheerio = require("cheerio");
const utils = require("./utils");
const c = require("./constant");
const ogs = require('open-graph-scraper');


const framePromoDetail = {
  selector: "#contentpromolain2",
  field: {
    title: "div.titleinside > h3",
    image: "div.keteranganinside > img",
    area: "div.area > b",
    periode: "div.periode",
  }
};

module.exports.PromisedFetch = function (promoURL, cookie) {
  return new Promise(function (resolve, reject) {
    let header = c.DEFAULT_HEADERS;
    header['Cookie'] = cookie;

    let URLParsed = urllib.parse(promoURL);
    let baseURL = URLParsed.protocol + "://" + URLParsed.hostname;

    console.log('[+] extractor ~> Loading url: ' + promoURL);

    request({
        url: promoURL,
        method: 'GET',
        headers: header,
        timeout: 5000
      }, function (error, response, body) {
        if (error) {
          // reject(error)
          console.log("[!] extractor ~> Got Error for: " + promoURL + " -> " + error);
          resolve(undefined)
        } else {
          if (body === undefined || response === undefined || error) {
            console.log("[!] got no data. url: " + promoURL + "\n" +
              "error: " + error + "\n" +
              "response: " + response + "\n" +
              "body: " + body + "\n"
            );
            console.log("[!] extractor ~> Error: " + promoURL);
            resolve(undefined)
          } else {
            if (response.statusCode === 200) {
              let $ = cheerio.load(body);

              let rule = {};
              let ruleExist = false;
              if (promoURL.includes("promo_detail.php")) {
                rule = framePromoDetail;
                ruleExist = true
              }

              if (ruleExist === false) {
                ogs({
                  'url': promoURL,
                  'timeout': 4000
                }, function (error, og) {
                  if (error) {
                    // reject(error)
                    console.log("[!] extractor ~> Error for OGS: " + promoURL + " err: " + error);
                    resolve(undefined)
                  } else {

                    let imgURL = "";
                    if (og.data.ogImage !== undefined) {
                      imgURL = og.data.ogImage.url
                    }

                    let siteDesc = "";
                    if (og.data.ogDescription !== undefined) {
                      siteDesc = og.data.ogDescription
                    }

                    console.log('[+] extractor ~> extracting data from url: ' + promoURL + " using OGS");
                    resolve({
                      url: promoURL,
                      title: utils.cleanString(og.data.ogTitle),
                      image: imgURL,
                      description: siteDesc,
                      area: "",
                      periode: ""
                    })
                  }
                });
              } else {
                console.log('[+] extractor ~> extracting data from url: ' + promoURL);
                let content = $(rule.selector);
                let image = baseURL + utils.cleanString($(content).find(rule.field.image).attr('src'));
                console.log('[+] extractor ~> resolving promise with data from url: ' + promoURL);

                resolve({
                  url: promoURL,
                  title: utils.cleanString($(content).find(rule.field.title).text().trim()),
                  image: image,
                  description: "",
                  area: utils.cleanString($(content).find(rule.field.area).text().trim()),
                  periode: utils.cleanString($(content).find(rule.field.periode).text().trim()),
                });
              }
            }
          }
        }
      }
    )
  })
};
