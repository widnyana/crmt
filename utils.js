const fs = require('fs');


module.exports.getMethods = obj => {
  let properties = new Set();
  let currentObj = obj;
  do {
    Object.getOwnPropertyNames(currentObj).map(item => properties.add(item));
  } while ((currentObj = Object.getPrototypeOf(currentObj)));
  return [...properties.keys()].filter(item => typeof obj[item] === "function");
};

module.exports.formatCookie = puppeteerCookie => {
  return puppeteerCookie.map(c => c.name + "=" + c.value).join("&");
};


/**
 *
 * @param {string} text
 * @returns {string}
 */
module.exports.cleanString = text => {
  if (text === undefined) {
    return ""
  }
  text = text.replace(/(\r\n\t|\t|\n|\r)/gm, "");
  text = text.replace(/\s/g, " ");
  return text
};


/**
 *
 * @param {string} filepath
 * @param {string} content
 */
module.exports.writeFile = (filepath, content) => {

  let buffer = new Buffer(content);

  fs.open(filepath, 'w', function (err, fd) {
    if (err) {
      throw 'could not open file: ' + err;
    }

    // write the contents of the buffer, from position 0 to the end, to the file descriptor returned in opening our file
    fs.write(fd, buffer, 0, buffer.length, null, function (err) {
      if (err) throw 'error writing file: ' + err;
      fs.close(fd, function () {
        console.log('wrote data successfully to ' + filepath);
      });
    });
  });

};
