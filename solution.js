"use strict";

const pp = require("puppeteer");
const utils = require("./utils");
const c = require("./constant");
const ext = require("./extractor");


async function getBrowser() {
  return pp.launch({
    headless: true,
    slowMo: 100,
    args: [`--window-size=${c.WIDTH},${c.HEIGHT}`]
  });
}


/**
 * Open a new browser, and perform extraction on each category
 *
 * @param {Browser} browser
 * @param {string} startURL
 * @param {string} elID
 * @param {string} title
 */
function extractCatOnNewTab(browser, startURL, elID, title) {
  return new Promise(async (resolve, reject) => {
    console.log("[+] solution ~> Begin extracting category: " + title);
    let results = [];
    let page = await browser.newPage();
    console.log("[+] solution ~> Opening start url");
    await page.goto(startURL);
    console.log("[+] solution ~> waiting selector found");
    await page.waitForSelector(c.SELECTOR_SUBPROMO);

    console.log("[+] solution ~> Clicking subpromo category: " + title);
    await page.click(c.SELECTOR_SUBPROMO_IMG + "#" + elID);
    console.log("[+] solution ~> waiting selector found");
    await page.waitForSelector(c.SELECTOR_SUBPROMO);

    // check for promo existence
    console.log("[+] solution ~> checking for promo existence");
    let promos = await page.$$(c.SELECTOR_PROMOLIST);
    if (promos.length < 1) {
      console.log("[!] no promo found");
      resolve({
        title: title,
        data: []
      })
    }

    const cookies = utils.formatCookie(await page.cookies());

    // get total pagination
    // hint: title="Page 5 of 5" from //div[@id='contentpromolain2']//table[@class='tablepaging']//a[@id='paging1']
    console.log("[!] solution ~> getting total pagination for: " + title);
    let totalPage = 0;
    let [tpElement] = await page.$x("//div[@id='contentpromolain2']//table[@class='tablepaging']//a[@id='paging1']");
    if (tpElement !== undefined) {
      let tpHandle = await tpElement.getProperty("title");
      let tpValue = await tpHandle.jsonValue();
      let match = tpValue.match(c.REGEX_TOTAL_PAGE);
      if (match) {
        totalPage = match[0];
      }
    }

    console.log("[+] solution ~> -> total page for category:" + title + " -> " + totalPage);
    if (totalPage < 1) {
      console.log("[!] solution ~> no data to extract.");
      resolve({
        title: title,
        data: []
      })
    }
    console.log("[+] solution ~> extracting links to detail promo ....");

    let promReq = [];

    // iterate the pagination
    for (let i = 0; i < totalPage; i++) {
      await page.waitForSelector(c.SELECTOR_SUBPROMO);
      const hrefs = await page.$$eval(c.SELECTOR_DETAILPROMO, as =>
        as.map(a => a.href)
      );

      for (let a = 0; a < hrefs.length; a++) {
        // fetch promodetail using request and return via promise
        promReq.push(ext.PromisedFetch(hrefs[a], cookies))
      }

      //navigate the pagination
      let nextPageLink = await page.$x(c.XPATH_SELECTOR_SUBPROMO_NEXT_PAGE);
      if (nextPageLink.length > 0) {
        console.log('[+] navigating to next page. i = ' + i);
        await nextPageLink[0].click();
      } else {
        console.log("[!] solution ~> no next page link found for: " + title + "i: " + i)
      }
    }

    // extract link to promo detail, and extract each promo detail.
    Promise.all(promReq)
      .then(res => {
        if (res !== null || true) {
          res.forEach(v => {
            if (v !== null && v !== undefined) {
              results.push(v)
            }
          });
        }
      })
      .catch(err => {
        console.log("error extracting" + err);
        resolve({
          title: title,
          data: []
        })
      })
      .then(() => {
        console.log("[+] solution ~> Done: " + title);
        resolve({
          title: title,
          data: results
        })
      });
  })
}

(async () => {
  let bag = {};
  let browser = await getBrowser();

  try {
    console.log("[!] solution ~> opening new tab");
    let page = await browser.newPage();
    await page.setViewport({width: c.WIDTH, height: c.HEIGHT});

    console.log("[+] solution ~> navigating to: " + c.START_URL);
    await page.goto(c.START_URL);
    await page.waitForSelector(c.SELECTOR_SUBPROMO);

    let catContainer = await page.$$(c.SELECTOR_SUBPROMO_IMG);
    const catTab = [];

    console.log("[+] solution ~> Iterating category container");

    for (const el of catContainer) {
      let elID = await (await el.getProperty("id")).jsonValue();
      let title = await (await el.getProperty("title")).jsonValue();

      // open each category in new tab
      catTab.push(await extractCatOnNewTab(browser, c.START_URL, elID, title));
    }


    Promise.all(catTab)
      .then(res => {
        console.log("[+] solution ~> res length =>>>> " + res.length);
        if (res.length >= 1) {
          res.forEach(el => {
            if (el !== null && el !== undefined) {
              console.log("-----------------> processing: " + el.title);
              bag[el['title']] = el['data'];
            }
          })
        }

        console.log("[+] solution ~>  writing result to file...");
        utils.writeFile('solution.json', JSON.stringify(bag));
      })
      .catch(err => {
        console.log("[!] solution ~> error processing" + err);
      });

    await browser.close();
  } catch (err) {
    console.log("got error :(");
    console.log(err);
    await browser.close();
  }
})();
