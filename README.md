# Bank Mega Scrapper

- I'm using headless `Google Chrome Browser` controlled by `puppeteer`.
- this source code will scrape each category on this url: `https://www.bankmega.com/promolainnya.php`




## Created using:
- nodejs v8.16.2
- Google Chrome Version 78.0.3904.97 (Official Build) (64-bit)
- macos High Sierra 10.13.6 (17G8030)


## Running

```shell script
yarn install
node solution.js
```

on ubuntu, you will need to run this on terminal

```shell script
sudo apt-get install node-gyp build-essential g++
```
