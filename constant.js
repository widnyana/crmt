module.exports = {
  SELECTOR_SUBPROMO: `div#subcatpromo`,
  SELECTOR_SUBPROMO_IMG: `div#subcatpromo > div > img`,
  XPATH_SELECTOR_SUBPROMO_NEXT_PAGE: `//div/table[@class='tablepaging']/tbody/tr/td[last()]/a[@class='page_promo_lain']`,
  SELECTOR_PROMOLIST: "ul#promolain",
  SELECTOR_DETAILPROMO: `#promolain > li > a`,
  START_URL: "https://www.bankmega.com/promolainnya.php",
  WIDTH: 1280,
  HEIGHT: 720,
  REGEX_TOTAL_PAGE: new RegExp("\\d{0,3}$"),
  DEFAULT_HEADERS: {
    'DNT': `1`,
    'User-Agent': `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36`,
    'Accept': `text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3`,
    'Accept-Language': `en-US,en;q=0.9,id;q=0.8,ms;q=0.7`,
  }
};
